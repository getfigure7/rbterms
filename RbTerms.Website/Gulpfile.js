﻿var gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    rename = require('gulp-rename'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    glob = require('glob'),
    es = require('event-stream');

gulp.task('default', function (done) {
    glob('./app/core/**.jsx', function (err, files) {
        if (err) done(err);
        
        var tasks = files.map(function (entry) {
            return browserify({ entries: [entry], extensions: ['.js', '.json', '.es6'] })
                .transform(babelify)
                .bundle()
                .pipe(source(entry))
                .pipe(rename({
                extname: '.bundle.js'
            }))
            .pipe(gulp.dest('../RbTerms.Website/public/'));
        });
        es.merge(tasks).on('end', done);
    })
});

gulp.watch('./app/core/**.jsx', ['default']);
gulp.watch('./app/core/**/**.jsx', ['default']);

gulp.watch('./app/components/**.jsx', ['default']);
gulp.watch('./app/components/**/**.jsx', ['default']);

gulp.watch('./app/globals/**.js', ['default']);
gulp.watch('./app/globals/**/**.js', ['default']);