﻿var $ = require('jquery');


class CurrentAuth {
    isAuthenticated() {
        if (sessionStorage.auth) return true;
        return false;
    }
    
    getToken() {
        if (!sessionStorage.auth) return 'UserIsGuest';
        return JSON.parse(sessionStorage.auth).Token;
    }
    
    getUser() {
        if (!sessionStorage.auth) {
            return {
                Name: 'Guest',
                Email: 'guest@noemail.com',
                IsAdmin: false,
                IsManager: false,
                RefererId: 0,
                Uuid: 'guest',
            };
        }
        return JSON.parse(sessionStorage.auth).User;

    }
    
    saveSession(token, user) {
        var data = {
            Token: token,
            User: user
        };
        sessionStorage.setItem('auth', JSON.stringify(data));
        return true;
    }
    
    updateUser(user) {
        console.log('updateUser');
        console.log(user);
        this.saveSession(this.getToken(), user);
    }
    
    removeSession() {
        sessionStorage.removeItem('auth');
        return true;
    }
}

module.exports.Auth = new CurrentAuth();
module.exports.CurrentAuth = new CurrentAuth();