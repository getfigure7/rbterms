﻿var $ = require('jquery');

var FlashMessage = require('../components/global/flash.jsx').FlashMessage;
// TODO: Finish this... this should allow us to flash mulitple messages to the interface
// Use localStorage/sessionStorage to make it so they must see it, as soon as it's been read, 
// it'll no longer show and be removed from the storage layer. 
class Flash {
    error(message) {
        localStorage.setItem('flashType', 'danger');
        localStorage.setItem('flashMessage', message);
    }
    
    getError() {
        return;
    }
}

module.exports.Flash = new Flash();