
class Config {
    getApiDomain() {
        return location.host=='rbterms.com' ? 'http://rbtermsapi.azurewebsites.net/' : 'http://localhost:49272/';
    }
    
    getRootDomain() {
        return location.host=='rbterms.com' ? 'http://rbterms.com/' : 'http://localhost:3001/';
    }
}

module.exports.Config = new Config();