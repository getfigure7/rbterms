var React = require('react');


var Error404 = React.createClass({
    render: function () {
        return (<div className="row"><h3 className="col-md-6 col-md-offset-3">Sorry, this page could not be found.</h3></div>);
    }
});

module.exports.Error404 = Error404;