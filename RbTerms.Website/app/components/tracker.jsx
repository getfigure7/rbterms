﻿var $ = require('jquery');
var React = require('react');

var Spinner = require('./global/spinner.jsx').Spinner;
var Config = require('../globals/config.js').Config;


var TrackerApp = React.createClass({
    getInitialState: function () {
        return {};
    },
    render: function () {
        return (
            <div className="container">
                <div>
                {this.props.children && React.cloneElement(this.props.children, {
                    setLoggedInState: this.setLoggedInState
                })}
                </div>
            </div>
       );
    }
});

var Click = React.createClass({
    getInitialState: function () {
        return {
            hasOid: true
        };
    },
    componentDidMount: function () {
        if (this.props.location.query.oid) {
            sessionStorage.setItem('oid', this.props.location.query.oid);
            sessionStorage.setItem('s1', this.props.location.query.s1 || '');
            sessionStorage.setItem('s2', this.props.location.query.s2 || '');
            sessionStorage.setItem('s3', this.props.location.query.s3 || '');
            sessionStorage.setItem('s4', this.props.location.query.s4 || '');
            sessionStorage.setItem('s5', this.props.location.query.s5 || '');
                
            window.location.href = '/confirm';
        } else {
            this.setState({ oidIsPresent: false });
        }
    },
    render: function () {
        if (!this.state.hasOid) return (<div className="col-md-6 col-md-offset-3 jumbotron">
            <h3>Opps.. An error occurred.</h3>
            The OID (?oid=) is missing from the url parameter.
        </div>);
        
        return (<div className="col-md-6 col-md-offset-3 jumbotron"><Spinner /></div>);
    }
});

module.exports.Click = Click;

var Confirm = React.createClass({
    getInitialState: function () {
        return {
            IsLoaded: false,
            FailedToLoad: false,
            Offer: {}
        };
    },
    componentDidMount: function () {
        this.loadOffer();
    },
    loadOffer: function () {
        if (sessionStorage.getItem('oid')) {
            var data = {
                oid: sessionStorage.getItem('oid'),
                s1: sessionStorage.getItem('s1'),
                s2: sessionStorage.getItem('s2'),
                s3: sessionStorage.getItem('s3'),
                s4: sessionStorage.getItem('s4'),
                s5: sessionStorage.getItem('s5'),
            };
            
            $.post(Config.getApiDomain() + 'Offers/GetOfferForLander', data, function (offer) {
                if (offer == null) {
                    this.setState({ IsLoaded: false, FailedToLoad: true });
                    return;
                }
                
                this.setState({ IsLoaded: true, Offer: offer });
            }.bind(this));
        } else {
            this.setState({ IsLoaded: false, FailedToLoad: true });
        }
    },
    handleConfirmClick: function () {
        sessionStorage.removeItem('oid');
        sessionStorage.removeItem('s1');
        sessionStorage.removeItem('s2');
        sessionStorage.removeItem('s3');
        sessionStorage.removeItem('s4');
        sessionStorage.removeItem('s5');
        
        window.location.href = this.state.Offer.Link;
    },
    render: function () {
        if (this.state.FailedToLoad) return (<div className="col-md-6 col-md-offset-3 jumbotron">
                <h3>Opps.. An error occurred.</h3>
                This offer is either expired, the 'oid' provided was incorrect or an error occurred. Please try again later.
            </div>);
        
        return this.state.IsLoaded ? (
            <div className="col-md-6 col-md-offset-3 jumbotron">
                <div>
                    <div className="col-md-5">
                        <img src={this.state.Offer.ImageLink} className="lander-image" />
                    </div>
                    <div className="col-md-7 center">
                        <h2>{this.state.Offer.Name}</h2>
                        <p className="description">{this.state.Offer.Description}</p>
                        <p className="requirements">{this.state.Offer.Requirements}</p>
                        <p><a className="btn btn-md btn-success" href="#" onClick={this.handleConfirmClick} role="button">{this.state.Offer.ButtonText}</a></p>
                    </div>
                </div>
            </div>) : (<div className="col-md-6 col-md-offset-3 jumbotron"><Spinner /></div>);
    }
});

module.exports.Confirm = Confirm;