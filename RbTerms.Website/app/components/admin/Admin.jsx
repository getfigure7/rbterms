﻿var $ = require('jquery');
var React = require('react');
var Link = require('react-router').Link;
var moment = require('moment-timezone');

var Formsy = require('formsy-react');
var Input = require('formsy-react-components').Input;
var Textarea = require('formsy-react-components').Textarea;
var Select = require('formsy-react-components').Select;
var RadioGroup = require('formsy-react-components').RadioGroup;
var Row = require('formsy-react-components').Row;
var FRC = require('formsy-react-components');
var Modal = require('react-bootstrap').Modal;

var Spinner = require('../global/spinner.jsx').Spinner;

var Config = require('../../globals/config.js').Config;

var Menu = React.createClass({
    render: function () {
        var selected = this.props.selected;
        
        return (<div className="admin">
            <div className="pull-left">
                <h4>Administration</h4>
            </div>
            <div className="pull-right">
                <ul className="nav nav-pills">
                    <li role="presentation" className={selected=='dashboard' ? 'active' : ''}><Link to="/manage/dashboard">Dashboard</Link></li>
                    <li role="presentation" className={selected=='users' ? 'active' : ''}><Link to="/manage/users">Users</Link></li>
                    <li role="presentation" className={selected=='offers' ? 'active' : ''}><Link to="/manage/offers">Offers</Link></li>
                </ul><br />
            </div>
        </div>);
    }
});

module.exports.User = React.createClass({
    getInitialState: function () {
        return { isLoaded: false, user: {} };
    },
    componentDidMount: function () {
        this.loadUser();
    },
    loadUser: function () {
        $.get(Config.getApiDomain() + 'Users/GetUser?uuid=' + this.props.params.uuid, function (data) {
            this.setState({ isLoaded: true, user: data });
        }.bind(this));
    },
    handleUpdate: function (form) {
        var data = {
            IsActive: form.IsActive == "active" ? true : false,
            Name: form.Name,
            Email: form.Email,
        };
        
        $.post(Config.getApiDomain() + 'Users/UpdateUser?uuid=' + this.props.params.uuid, data, function (data) {
            console.log(data);
            alert('Updated user');
        }.bind(this));
    },
    render: function () {
        var statusOptions = [
            { value: 'active', label: 'Active' },
            { value: 'inactive', label: 'Inactive' }
        ];
        
        var status = 'inactive';
        if (this.state.user.IsActive == true) {
            status = 'active';
        }
        
        var user = this.state.isLoaded ? (
                    <div>
                        <h3>Manage: {this.state.user.Name}</h3>
                        
                        <div className="panel panel-default">
                            <div className="panel-heading">Edit user</div>
                            <div className="panel-body">
                                <div className="col-md-8 col-md-offset-2">
                                <Formsy.Form ref="form" onValidSubmit={this.handleUpdate}>
                                    <RadioGroup
                                        name="IsActive"
                                        value={status}
                                        label="Status"
                                        help="This controls the ability for users to login or not."
                                        options={statusOptions}
                                    />
                        
                                    <Input name="Name" id="Name" label="Name" type="text"
                                        value={this.state.user.Name}
                                        validations={{
                                            minLength: 5,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter your name.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} required />
                                        
                                    <Input name="Email" id="Email" label="Email" type="text"
                                        value={this.state.user.Email}
                                        validations={{
                                            isEmail: true,
                                            minLength: 1,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            isEmail: 'Please enter your valid email address.',
                                            minLength: 'You must enter at least 3 characters.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} />
                                    
                                    <div className="pull-right">
                                        <input className="btn btn-md btn-primary subscribe-btn" formNoValidate={true} type="submit" value="Save" />
                                    </div>
                                </Formsy.Form>
                                </div>
                            </div>
                        </div>
                    </div>) : (<Spinner />);
     
        return (
            <div>
                <div className="row">
                    <Menu selected="users" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/users">List users</Link></li>
                            <li><Link to="/manage/users/create">Create user</Link></li>
                        </ul>
                    </div>
                    
                    <div className="col-md-10">
                    {user}
                    </div>
                </div>
        </div>);
    }
});

module.exports.CreateUser = React.createClass({
    getInitialState: function () {
        return { created: false };
    },
    handleSubmit: function (form) {
        $.post(Config.getApiDomain() + 'Users/CreateUser', form, function (data) {
            this.setState({ created: true });
        }.bind(this));
    },
    handleReset: function () {
        const formsy = this.refs.form.refs.formsy;
        formsy.reset();
    },
    render: function () {
        if (this.state.created) {
            return (
                <div>
                    <div className="row">
                        <Menu selected="users" />
                    </div>
                    <div className="row">
                        <div className="col-md-2">
                            <ul className="nav nav-pills nav-stacked">
                                <li><Link to="/manage/users">List Users</Link></li>
                                <li><Link to="/manage/users/create">Create User</Link></li>
                            </ul>
                        </div>
                        <div className="col-md-10">
                            Success!
                        </div>
                    </div>
            </div>);
        }

        return (
            <div>
                <div className="row">
                    <Menu selected="users" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/users">List Users</Link></li>
                            <li><Link to="/manage/users/create">Create User</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-10">
                        <div className="panel panel-default">
                            <div className="panel-heading">Create user</div>
                            <div className="panel-body">
                                <div className="col-md-8 col-md-offset-2">
                                <Formsy.Form ref="form" onValidSubmit={this.handleSubmit}>
                                    <Input layout="vertical" name="name" id="name" label="Name" type="text"
                                        validations={{
                                            minLength: 5,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter your name.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} required />
                                        
                                    <Input layout="vertical" name="email" id="email" label="Email" type="text"
                                        validations={{
                                            isEmail: true,
                                            minLength: 1,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            isEmail: 'Please enter your valid email address.',
                                            minLength: 'You must enter at least 3 characters.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} required />
                                        
                                    <Input layout="vertical" name="password" id="password" label="Password" type="password" required />
                                    <Input layout="vertical" name="confirm-password" id="confirm-password" label="Confirm Password" type="password"
                                        validations="equalsField:password"
                                        validationErrors={{
                                            equalsField: 'Confirm Password must match your password.'
                                        }} required />
                                    
                                    <div className="pull-right">
                                        <input className="btn btn-md btn-primary subscribe-btn" formNoValidate={true} type="submit" value="Create" />
                                    </div>
                                </Formsy.Form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>);
    }
});

module.exports.Users = React.createClass({
    getInitialState: function () {
        return { isLoaded: false, Users: [] };
    },
    componentDidMount: function () {
        $.get(Config.getApiDomain() + 'Users/GetAllUsers', function (response) {
            this.setState({ isLoaded: true, Users: response });
        }.bind(this));
    },
    render: function() {
        var userNodes = this.state.Users.map(function(user, key) {
            var createdAt = moment.tz(user.CreatedAt, 'America/New_York').format('MM/DD/YYYY hA');
            
            return (
                <tr key={key}>
                    <td><Link to={"/manage/user/" + user.Uuid}>{user.Name}</Link></td>
                    <td>{user.Email}</td>
                    <td>{createdAt}</td>
                    <td>{user.UpdatedAt}</td>
                    <td><Link to={"/manage/user/" + user.Uuid}><i className="fa fa-hand-o-right"></i> Open</Link>
                     </td>
                </tr>
            );
        }.bind(this));
        
        var users = this.state.isLoaded ? (<div className="table-responsive">
                        <table className="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            {userNodes}
                        </tbody>
                      </table>
                      </div>) : (<Spinner />);
        
        return (
        <div>
                <div className="row">
                    <Menu selected="users" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/users">List Users</Link></li>
                            <li><Link to="/manage/users/create">Create User</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-10">
            <div className="panel panel-default">
                <div className="panel-heading">Manage Users</div>
                {users}
            </div>
            </div>
            </div>
        </div>);
    }
});


module.exports.Offer = React.createClass({
    getInitialState: function () {
        return { isLoaded: false, Offer: {} };
    },
    componentDidMount: function () {
        this.loadOffer();
    },
    loadOffer: function () {
        $.get(Config.getApiDomain() + 'Offers/GetOffer?uuid=' + this.props.params.uuid, function (data) {
            this.setState({ isLoaded: true, Offer: data });
        }.bind(this));
    },
    handleUpdate: function (form) {
        var data = {
            IsActive: form.IsActive == 'active' ? true : false,
            Name: form.Name,
            Description: form.Description,
            Requirements: form.Requirements,
            ButtonText: form.ButtonText,
            ImageLink: form.ImageLink,
            Link: form.Link,
        };
        
        $.post(Config.getApiDomain() + 'Offers/UpdateOffer?uuid=' + this.props.params.uuid, data, function (data) {
            console.log(data);
            alert('Updated offer');
        }.bind(this));
    },
    render: function () {
        var statusOptions = [
            { value: 'active', label: 'Active' },
            { value: 'inactive', label: 'Inactive' }
        ];
        
        var status = 'inactive';
        if (this.state.Offer.IsActive == true) {
            status = 'active';
        }
        
        var offer = this.state.isLoaded ? (
                    <div>
                        <h3>Manage: {this.state.Offer.Name}</h3>
                        
                        <div className="panel panel-default">
                            <div className="panel-heading">Tracking/Offer Link</div>
                            <div className="panel-body">
                                {Config.getRootDomain()}click?oid={this.state.Offer.Uuid}
                            </div>
                        </div>
                        
                        <div className="panel panel-default">
                            <div className="panel-heading">Edit offer</div>
                            <div className="panel-body">
                                <div className="col-md-8 col-md-offset-2">
                                <Formsy.Form ref="form" onValidSubmit={this.handleUpdate}>
                                    <RadioGroup
                                        name="IsActive"
                                        value={status}
                                        label="Status"
                                        help="This controls the status of this offer."
                                        options={statusOptions}
                                    />
                        
                                    <Input name="Name" id="name" label="Name" type="text"
                                        value={this.state.Offer.Name}
                                        validations={{
                                            minLength: 5,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter the offer name.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} required />
                                        
                                    <Textarea
                                        value={this.state.Offer.Requirements}
                                        rows={3}
                                        cols={40}
                                        name="Requirements"
                                        label="Requirements"
                                        placeholder="Enter the requirements text to be shown on the landing page for this offer."
                                        help="This is the requirements text shown on the landing page for this offer."
                                        validations="minLength:10"
                                        validationErrors={{
                                            minLength: 'Please provide at least 10 characters.'
                                        }}
                                        required
                                    />
                                    
                                    <Textarea
                                        value={this.state.Offer.Description}
                                        rows={3}
                                        cols={40}
                                        name="Description"
                                        label="Description"
                                        placeholder="Enter the description text to be shown on the landing page for this offer."
                                        help="This is the description text shown on the landing page for this offer."
                                        validations="minLength:10"
                                        validationErrors={{
                                            minLength: 'Please provide at least 10 characters.'
                                        }}
                                        required
                                    />
                                    
                                    <Input name="ButtonText" id="buttonText" label="Confirm Button" type="text"
                                        value={this.state.Offer.ButtonText}
                                        help="This is the text shown on the button for the confirmation functionality to redirect users onto the advertiser link."
                                        value="I Accept"
                                        validations={{
                                            minLength: 3,
                                            maxLength: 20
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter the button text.',
                                            maxLength: 'You cannot enter more than 20 characters.'
                                        }} required />
                                        
                                    <Input value={this.state.Offer.ImageLink}
                                        id="ImageLink"
                                        name="ImageLink"
                                        label="Image Link"
                                        placeholder="http://"
                                        help="This is the URL to the image shown on the offer landing page."
                                        type="text"
                                        validations={{
                                            minLength: 12,
                                            maxLength: 200,
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter an image link (at least 12 characters).',
                                            maxLength: 'The image link cannot be longer than 200 characters.'
                                        }} required />
                                    
                                    <Row rowClassName="highlight_row blue">
                                        <Input
                                            value={this.state.Offer.Link}
                                            id="link"
                                            name="Link"
                                            label="Advertiser Link"
                                            placeholder="http://"
                                            help="This is the URL users are redirected to when clicking on the confirm button."
                                            type="text"
                                            validations={{
                                                minLength: 12,
                                                maxLength: 200,
                                            }}
                                            validationErrors={{
                                                minLength: 'Please enter an advertiser link (at least 12 characters).',
                                                maxLength: 'The advertiser link cannot be longer than 200 characters.'
                                            }} required />
                                    </Row>
                                    
                                    <div className="pull-right">
                                        <input className="btn btn-md btn-primary subscribe-btn" formNoValidate={true} type="submit" value="Save" />
                                    </div>
                                </Formsy.Form>
                                </div>
                            </div>
                        </div>
                    </div>) : (<Spinner />);
     
        return (
            <div>
                <div className="row">
                    <Menu selected="offers" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/offers">List Offers</Link></li>
                            <li><Link to="/manage/offers/create">Create Offer</Link></li>
                        </ul>
                    </div>
                    
                    <div className="col-md-10">
                    {offer}
                    </div>
                </div>
        </div>);
    }
});

module.exports.CreateOffer = React.createClass({
    getInitialState: function () {
        return { created: false };
    },
    handleSubmit: function (form) {
        $.post(Config.getApiDomain() + 'Offers/CreateOffer', form, function (data) {
            this.setState({ created: true });
        }.bind(this));
    },
    handleReset: function () {
        const formsy = this.refs.form.refs.formsy;
        formsy.reset();
    },
    render: function () {
        if (this.state.created) {
            return (
                <div>
                    <div className="row">
                        <Menu selected="offers" />
                    </div>
                    <div className="row">
                        <div className="col-md-2">
                            <ul className="nav nav-pills nav-stacked">
                                <li><Link to="/manage/offers">List Offers</Link></li>
                                <li><Link to="/manage/offers/create">Create Offer</Link></li>
                            </ul>
                        </div>
                        <div className="col-md-10">
                            Success!
                        </div>
                    </div>
            </div>);
        }

        return (
            <div>
                <div className="row">
                    <Menu selected="offers" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/offers">List Offers</Link></li>
                            <li><Link to="/manage/offers/create">Create Offer</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-10">
                        <div className="panel panel-default">
                            <div className="panel-heading">Create offer</div>
                            <div className="panel-body">
                                <div className="col-md-8 col-md-offset-2">
                                <Formsy.Form ref="form" onValidSubmit={this.handleSubmit}>
                                    <Input name="Name" id="name" label="Name" type="text"
                                        validations={{
                                            minLength: 5,
                                            maxLength: 200
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter the offer name.',
                                            maxLength: 'You cannot enter more than 200 characters.'
                                        }} required />
                                        
                                    <Textarea
                                        rows={3}
                                        cols={40}
                                        name="Requirements"
                                        label="Requirements"
                                        placeholder="Enter the requirements text to be shown on the landing page for this offer."
                                        help="This is the requirements text shown on the landing page for this offer."
                                        validations="minLength:10"
                                        validationErrors={{
                                            minLength: 'Please provide at least 10 characters.'
                                        }}
                                        required
                                    />
                                    
                                    <Textarea
                                        rows={3}
                                        cols={40}
                                        name="Description"
                                        label="Description"
                                        placeholder="Enter the description text to be shown on the landing page for this offer."
                                        help="This is the description text shown on the landing page for this offer."
                                        validations="minLength:10"
                                        validationErrors={{
                                            minLength: 'Please provide at least 10 characters.'
                                        }}
                                        required
                                    />
                                    
                                    <Input name="ButtonText" id="buttonText" label="Confirm Button" type="text"
                                        help="This is the text shown on the button for the confirmation functionality to redirect users onto the advertiser link."
                                        value="I Accept"
                                        validations={{
                                            minLength: 3,
                                            maxLength: 20
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter the button text.',
                                            maxLength: 'You cannot enter more than 20 characters.'
                                        }} required />
                                    
                                    <Input id="ImageLink" name="ImageLink"
                                        label="Image Link"
                                        placeholder="http://"
                                        help="This is the URL to the image shown on the offer landing page."
                                        type="text"
                                        validations={{
                                            minLength: 12,
                                            maxLength: 200,
                                        }}
                                        validationErrors={{
                                            minLength: 'Please enter an image link (at least 12 characters).',
                                            maxLength: 'The image link cannot be longer than 200 characters.'
                                        }} required />
                                    
                                    <Row rowClassName="highlight_row blue">
                                        <Input
                                            id="link"
                                            name="Link"
                                            label="Advertiser Link"
                                            placeholder="http://"
                                            help="This is the URL users are redirected to when clicking on the confirm button."
                                            type="text"
                                            validations={{
                                                minLength: 12,
                                                maxLength: 200,
                                            }}
                                            validationErrors={{
                                                minLength: 'Please enter an advertiser link (at least 12 characters).',
                                                maxLength: 'The advertiser link cannot be longer than 200 characters.'
                                            }} required />
                                    </Row>
                                    
                                    <div className="pull-right">
                                        <input className="btn btn-md btn-primary subscribe-btn" formNoValidate={true} type="submit" value="Create" />
                                    </div>
                                </Formsy.Form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>);
    }
});

module.exports.Offers = React.createClass({
    getInitialState: function () {
        return { isLoaded: false, Offers: [] };
    },
    componentDidMount: function () {
        $.get(Config.getApiDomain() + 'Offers/GetAllOffers', function (response) {
            this.setState({ isLoaded: true, Offers: response });
        }.bind(this));
    },
    render: function() {
        var offerNodes = this.state.Offers.map(function(offer, key) {
            var createdAt = moment.tz(offer.CreatedAt, 'America/New_York').format('MM/DD/YYYY hA');
            var updatedAt = moment.tz(offer.UpdatedAt, 'America/New_York').format('MM/DD/YYYY hA');
            
            return (
                <tr key={key}>
                    <td><Link to={"/manage/offer/" + offer.Uuid}>{offer.Name}</Link></td>
                    <td>{offer.IsActive ? (<span className="label label-success">Active</span>) : (<span className="label label-warning">Inactive</span>)}</td>
                    <td>{createdAt}</td>
                    <td>{updatedAt}</td>
                    <td><Link to={"/manage/offer/" + offer.Uuid}><i className="fa fa-hand-o-right"></i> Open</Link>
                     </td>
                </tr>
            );
        }.bind(this));
        
        var offers = this.state.isLoaded ? (<div className="table-responsive">
                        <table className="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            {offerNodes}
                        </tbody>
                      </table>
                      </div>) : (<Spinner />);
        
        return (
        <div>
                <div className="row">
                    <Menu selected="offers" />
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <ul className="nav nav-pills nav-stacked">
                            <li><Link to="/manage/offers">List Offers</Link></li>
                            <li><Link to="/manage/offers/create">Create Offer</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-10">
            <div className="panel panel-default">
                <div className="panel-heading">Manage Offers</div>
                {offers}
            </div>
            </div>
            </div>
        </div>);
    }
});

module.exports.Dashboard = React.createClass({
    render: function () {
        return (<div>
            <div className="row">
                    <Menu selected="dashboard" />
                </div>
                <div className="row">
                    Use the tabs at the top right to navigate the admin.
                </div>
        </div>);
    }
});