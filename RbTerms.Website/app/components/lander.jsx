﻿var $ = require('jquery');
var React = require('react');

var Spinner = require('./global/spinner.jsx').Spinner;
var Config = require('../globals/config.js').Config;


var LanderApp = React.createClass({
    getInitialState: function () {
        return {};
    },
    render: function () {
        return (
            <div className="container">
                <div>
                {this.props.children && React.cloneElement(this.props.children, {
                    setLoggedInState: this.setLoggedInState
                })}
                </div>
            </div>
       );
    }
});

var Lander = React.createClass({
    getInitialState: function () {
        return {
            StrippingSubids: true,
            IsLoaded: false,
            FailedToLoad: false,
            Offer: {}
        };
    },
    componentDidMount: function () {
        this.stripSubids();
        this.loadOffer();
        
        console.log(sessionStorage);
    },
    stripSubids: function () {
        if (this.props.location.query.s1 ||
            this.props.location.query.s2 ||
            this.props.location.query.s3 ||
            this.props.location.query.s4 ||
            this.props.location.query.s5) {
                console.log(sessionStorage.getItem('s1'));
                sessionStorage.setItem('s1', this.props.location.query.s1 || '');
                sessionStorage.setItem('s2', this.props.location.query.s2 || '');
                sessionStorage.setItem('s3', this.props.location.query.s3 || '');
                sessionStorage.setItem('s4', this.props.location.query.s4 || '');
                sessionStorage.setItem('s5', this.props.location.query.s5 || '');
                    
                window.location.href = Config.getRootDomain() + '?oid=' + this.props.location.query.oid;
        }
        
        this.setState({ StrippingSubids: false });
    },
    loadOffer: function () {
        var data = {
            oid: this.props.location.query.oid,
            s1: this.props.location.query.s1 || '',
            s2: this.props.location.query.s2 || '',
            s3: this.props.location.query.s3 || '',
            s4: this.props.location.query.s4 || '',
            s5: this.props.location.query.s5 || '',
        };
        
        $.post(Config.getApiDomain() + 'Offers/GetOfferForLander', data, function (offer) {
            if (offer == null) {
                this.setState({ IsLoaded: false, FailedToLoad: true });
                return;
            }
            
            this.setState({ IsLoaded: true, Offer: offer });
        }.bind(this));
    },
    handleConfirmClick: function () {
        window.location.href = this.state.Offer.Link;
    },
    render: function () {
        if (this.state.StrippingSubids) return (<div></div>);
        
        if (this.state.FailedToLoad) return (<div className="col-md-6 col-md-offset-3 jumbotron">
                This offer is either expired, the 'oid' in the URL is incorrect or an error occurred.
            </div>);
        
        return this.state.IsLoaded ? (
            <div className="col-md-6 col-md-offset-3 jumbotron">
                <div>
                    <div className="col-md-5">
                        <img src={this.state.Offer.ImageLink} className="lander-image" />
                    </div>
                    <div className="col-md-7 center">
                        <h2>{this.state.Offer.Name}</h2>
                        <p className="description">{this.state.Offer.Description}</p>
                        <p className="requirements">{this.state.Offer.Requirements}</p>
                        <p><a className="btn btn-md btn-success" href="#" onClick={this.handleConfirmClick} role="button">{this.state.Offer.ButtonText}</a></p>
                    </div>
                </div>
            </div>) : (<div className="col-md-6 col-md-offset-3 jumbotron"><Spinner /></div>);
    }
});

module.exports.Lander = Lander;