﻿var $ = require('jquery');
var React = require('react');
var Link = require('react-router').Link;

var Spinner = require('./spinner.jsx').Spinner;


module.exports.OfferTile = React.createClass({
    getInitialState: function () {
        return { clicked: false, completed: false, reported: false, offer: this.props.offer };
    },
    handleTileClick: function () {
        this.setState({ clicked: true });
    },
    handleComplete: function (uuid, e) {
        $.post('http://soundznew.dev/Offers/CompleteOffer', { OfferUuid: uuid }, function (data) {
            this.setState({ clicked: true, completed: true });
        }.bind(this));
    },
    handleReported: function () {
        this.setState({ clicked: true, completed: false, reported: true });
    },
    render: function () {
        if (!this.state.clicked) {
            return (<div className="col-md-4">
                <div className="tile offer" onClick={this.handleTileClick}>
                    <div className="pull-left">
                        <h4 className="title"><a href="">{this.state.offer.Name}</a></h4>
                    </div>
                    <div className="pull-right">
                        <i className="fa fa-external-link" />
                    </div><br /><br />
                    
                    <div className="center">
                        <h5>Earn {this.state.offer.Points} RavePoints</h5>
                    </div>
                </div>
            </div>);
        }
        
        if (this.state.completed) {
            return (<div className="col-md-4">
                <div className="tile offer completed">
                    <h4 className="title">{this.state.offer.Name}</h4><br />
                    
                    <div className="center">
                        Great Job, yo! You'll be awarded {this.state.offer.Points} RavePoints if the advertiser pays us.
                        If not, we'll still enter you into our <a href="">Monthly Jackpot</a>.
                    </div><br />
                </div>
            </div>);
        }
        
        if (this.state.reported) {
            return (<div className="col-md-4">
                <div className="tile offer reported">
                    <h4 className="title">{this.state.offer.Name}</h4><br />
                    
                    <div className="center">
                        Sorry this offer didn't work for you. :( We'll check it out.<br /><br />
                        
                        Thanks for reporting it.
                    </div><br />
                </div>
            </div>);
        }
        
        return (<div className="col-md-4">
                <div className="tile offer clicked">
                    <h4 className="title">{this.state.offer.Name}</h4><br />
                    
                    <div className="center">
                        Please click 'Complete' once you've finished with the offer.
                    </div><br />
                    
                    <div className="pull-left">
                        <a href="#" className="btn btn-sm btn-danger" onClick={this.handleReported}>Report</a>
                    </div>
                    <div className="pull-right">
                        <a href="#" className="btn btn-sm btn-success" onClick={this.handleComplete.bind(this, this.state.offer.Uuid)}>Complete</a>
                    </div>
                </div>
            </div>);
    },
});

module.exports.AdminOfferTile = React.createClass({
    getInitialState: function () {
        return { clicked: false, offer: this.props.offer };
    },
    handleTileClick: function () {
        this.setState({ clicked: true });
    },
    render: function () {
        return (<div className="col-md-4">
            <div className="tile offer">
                <div className="center">
                    <h4 className="title"><a href="">{this.state.offer.Name}</a></h4>
                </div><br /><br />
                
                <div className="center">
                    <Link to={"/admin/offer/" + this.state.offer.Uuid}>Edit Offer</Link><br />
                    <a href="">Quick Inactive</a>
                </div>
            </div>
        </div>);
    },
});