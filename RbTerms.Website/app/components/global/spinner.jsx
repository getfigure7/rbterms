﻿var React = require('react');


module.exports.Spinner = React.createClass({
    render: function () {
        var text = this.props.text || "Please wait..";
        
        return (<span className="spinner"><i className={this.props.style || "fa fa-spinner fa-spin pull-right"}></i> {text}</span>);
    },
});