var React = require('react');

// This component works with the app/globals/flash.js global utility class
module.exports.FlashMessage = React.createClass({
    render: function () {
        return (<div class={"alert alert-" + this.props.type}>{this.props.message}</div>);
    },
});