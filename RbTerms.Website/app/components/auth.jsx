﻿var $ = require('jquery');
var React = require('react');
var Link = require('react-router').Link;
var browserHistory = require('react-router').browserHistory;

var Formsy = require('formsy-react');
var Input = require('formsy-react-components').Input;

var Spinner = require('./global/spinner.jsx').Spinner;

var Auth = require('../globals/auth.js').Auth;
var Config = require('../globals/config.js').Config;

module.exports.Login = React.createClass({
    getInitialState: function () {
        return {
            isLoggingIn: false,
        }
    },
    handleSubmit: function (form) {
        this.setState({ isLoggingIn: true });

        var creds = { Email: form.email, Password: form.password };
        
        $.post(Config.getApiDomain() + 'Authentication/RequestToken', creds, function (data) {
            console.log('login user data var:');
            console.log(data);
            if (('Token' in data && 'User' in data) && Auth.saveSession(data.Token, data.User)) {
                this.props.setLoggedInState(true, data.User);
                this.setState({ isLoggingIn: false });
                
                var { location } = this.props;
                if (location.state && location.state.nextPathname) {
                    browserHistory.replace(location.state.nextPathname);
                } else {
                    browserHistory.replace('/manage/dashboard');
                }
            }
        }.bind(this));
    },
    render: function() {
        return (
            <div className="row">
                <div className="col-md-6 col-md-offset-3">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <Formsy.Form className="login-form" onSubmit={this.handleSubmit} ref="form">
                            <fieldset>
                                <legend>Please login into your account..</legend>

                                <Input name="email" id="email" label="Email" type="text" required />
                                <Input name="password" id="password" label="Password" type="password" required />

                                <div className="pull-right">
                                    {!this.state.isLoggingIn ?
                                        <input className="btn btn-lg btn-primary subscribe-btn" formNoValidate={false} type="submit" defaultValue="Login" /> :
                                        <Spinner /> }
                                </div>
                            </fieldset>
                            </Formsy.Form>
                        </div>
                    </div>
                </div>
            </div>);
    }
});

module.exports.Logout = React.createClass({
    componentDidMount: function () {
        if (Auth.removeSession()) {
            this.props.setLoggedInState(false, Auth.getUser());
        
            browserHistory.replace('/manage/login');
        }
    },
    render: function () {
        return (<Spinner />);
    }
});