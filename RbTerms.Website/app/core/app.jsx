﻿var React = require('react');
var ReactDOM = require('react-dom');

var Router = require('react-router').Router;
var Route = require('react-router').Route;
var Link = require('react-router').Link;
var browserHistory = require('react-router').browserHistory;

var Nav = require('react-bootstrap').Nav;
var Navbar = require('react-bootstrap').Navbar;
var NavItem = require('react-bootstrap').NavItem;

var Auth = require('../globals/auth.js').Auth;
var Config = require('../globals/config.js').Config;

var $ = require('jquery');

$.ajaxPrefilter(function(options) {
    if (!options.beforeSend) {
        options.beforeSend = function (xhr) {
            if (Auth.isAuthenticated()) xhr.setRequestHeader('Authorization', Auth.getToken());
        }
    }
});

var App = React.createClass({
    contextTypes: {
        router: React.PropTypes.object
    },
    getInitialState: function () {
        return { loggedIn: Auth.isAuthenticated(), currentUser: Auth.getUser() };
    },
    componentWillMount: function () {        
        if (Auth.isAuthenticated()) this.updateCurrentUser();
    },
    setLoggedInState: function (isLoggedIn, user) {
        this.setState({
            loggedIn: isLoggedIn,
            currentUser: user
        });
    },
    updateCurrentUser: function () {
        $.get(Config.getApiDomain() + 'Users/GetCurrentUser', function (user, status) {
            if (('IsActive' in user && user.IsActive == true) && status == 'success') {
                console.log('updateCurrentUser');
                console.log(user);
                Auth.updateUser(user);
                this.setLoggedInState(true, user);
            } else {
                Auth.updateUser({});
                this.setLoggedInState(false, {});
                browserHistory.replace('/manage/logout');
            }
        }.bind(this));
    },
    render: function () {
        var backgroundSwap = 'background-image';
        
        return (
            <div className="container">
                <Navbar bsClass="navbar" bsStyle="default" fluid={true} staticTop={false}>
                    <Navbar.Header>
                    <Navbar.Brand>
                       RbTerms
                    </Navbar.Brand>
                    <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                    <Nav pullLeft>
                        
                    </Nav>
                    
                    <Nav pullRight>
                        { this.state.loggedIn ? (<li><Link to="/manage/dashboard">Dashboard</Link></li>) : ""}
                        <li>
                            { this.state.loggedIn ? (
                                <Link to="/manage/logout"><i className="fa fa-sign-out"></i></Link>
                            ) : (
                                <Link to="/manage/login">Login</Link>
                            )}
                        </li>
                    </Nav>
                    </Navbar.Collapse>
                </Navbar>
                
                <div className={"content " + backgroundSwap}>
                {this.props.children && React.cloneElement(this.props.children, {
                    setLoggedInState: this.setLoggedInState
                })}
                </div>
                
            </div>
       );
    }
});

function requireAuth(nextState, replace) {
    if (!Auth.isAuthenticated()) {
        replace({
            pathname: '/manage/login',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}

function requireAdmin(nextState, replace) {
    var user = Auth.getUser();
    // TODO: Make this better.. should log when someone tries to go to admin when they're isAdmin==false.
    if (!user.IsAdmin) {
        replace('/manage/logout');
    }
}

function requireGuest(nextState, replace) {
    if (Auth.isAuthenticated()) {
        replace('/manage/dashboard');
    }
}

var TrackerApp = require('../components/tracker.jsx').TrackerApp;
var Click = require('../components/tracker.jsx').Click;
var Confirm = require('../components/tracker.jsx').Confirm;

var Login = require('../components/auth.jsx').Login;
var Logout = require('../components/auth.jsx').Logout;
var Admin = require('../components/admin/admin.jsx');
var Error404 = require('../components/errors.jsx').Error404;

ReactDOM.render((
  <Router history={browserHistory}>
    <Route component={App}>
        <Route path="manage/dashboard" component={Admin.Dashboard} onEnter={requireAuth} />
        
        <Route path="manage/users" component={Admin.Users} onEnter={requireAuth} />
        <Route path="manage/users/create" component={Admin.CreateUser} onEnter={requireAuth} />
        <Route path="manage/user/:uuid" component={Admin.User} onEnter={requireAuth} />
        
        <Route path="manage/offers" component={Admin.Offers} onEnter={requireAuth} />
        <Route path="manage/offers/create" component={Admin.CreateOffer} onEnter={requireAuth} />
        <Route path="manage/offer/:uuid" component={Admin.Offer} onEnter={requireAuth} />
        
        <Route path="manage/login" component={Login} onEnter={requireGuest} />
        <Route path="manage/logout" component={Logout} onEnter={requireAuth} />
        
        <Route path="manage/*" component={Error404} />
    </Route>
    <Route component={TrackerApp}>
        <Route path="/click" component={Click} />
        <Route path="/confirm" component={Confirm} />
        
        <Route path="*" component={Error404} />
    </Route>
  </Router>
), document.getElementById('page-body'));