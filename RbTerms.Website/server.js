﻿var express = require('express');
var path = require('path');

var geoip = require('geoip-lite');
var requestIp = require('request-ip');

var app = express();
var port = 3001;

app.use(express.static('public'));
app.use(requestIp.mw());

app.get('*', function (req, res) {
    
    //console.log(req.clientIp);
    
    //var geo = geoip.lookup(req.clientIp);

    //console.log(geo);
    
    res.sendFile(path.join(__dirname + '/app/index.html'))
});

/*app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/app/index.html'));
});*/

app.listen(port, function () {
    console.log('RbTermsNode app listening on port %s!', port);
    console.log('http://localhost:%s', port);
});